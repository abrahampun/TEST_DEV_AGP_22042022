# TEST_DEV_AGP_22042022
# Título del Proyecto
a
_Examen/prueba para desarolllador toka_

### Informacion del proyecto 📋

_Este repositorio contiene un proyecto ASP.NET CORE 3.1+_

En el cual se cuentran los endpoints solicitando en el examen de toka, un crud en el cual se implementaron algunos Procedimientos de almacenados que se proporcionaron al inicio.

## FrodEnd ⚙️

_El frondEnd, fue realizado con Angular 13, en el cual se consume la api de este proyecto, asi como algunos otros que se proporcionaron para la prueba_
El proyecto se encuentra en el siguiente repositorio https://gitlab.com/abrahampun/TEST_DEV_AGP_22042022-frond

### Ajustes 🔧

_Se realizaron algunos ajustes especialmente en la procedimiento de almacenado_
_Dentro de este proyecto existe una carpeta llamada script, donde encontrara el sql modificado que se me proporciono_

```
*ajuste una validación en crear, que intenta validar que no existiera el rfc similar, estaba haciendo lo contrario.
*ajuste una validación en actualizar, que intentaba validar que existiera el registro, estaba haciendo lo contrario
*en el procedimiento almacenado actualizar, se identificó que la fecha de modificación, no se estaba actualizando.
*ajuste en procedimiento almacenado actualizar, se estaba saltando la validación de comprobar, si el rfc ya existía, ahora solo aplica, para el id correspondiente.
```

## Ajustes no realizados/posibles mejoras ⚙️

_Se identificaron algunas mejoras que se pudieran hacer, pero ya no se alcanzo a realizar por tiempo_
```
-Generar un Procedimiento almacenado para obtener los datos, así manejar las posibles validaciones en sql
-Agregar nuevos campos a las tablas, UsuarioEliminar,FechaElimina,UsuarioModifica
-realizar ajuste todo los sp, que envían id, para evitar enviarlo atreves
-Falto realizar auth propia del api
```
