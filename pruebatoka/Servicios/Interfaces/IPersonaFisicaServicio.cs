﻿using Microsoft.AspNetCore.Mvc;
using pruebatoka.dto;
using pruebatoka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebatoka.Servicios.Interfaces
{
    public interface IPersonaFisicaServicio
    {
        public Task<ActionResult<IEnumerable<PersonaFisica>>> Obtener();
        public PersonaFisica ObtenerPorId(int id);
        public RespuestaGenericaDto Insertar(PersonaFisicaInsertDto data);
        public RespuestaGenericaDto Actualizar(PersonaFisicaActualizarDto data);
        public RespuestaGenericaDto Eliminar(int id);
    }
}
