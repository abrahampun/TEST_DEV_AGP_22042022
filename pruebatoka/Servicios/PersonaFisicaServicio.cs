﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using pruebatoka.Data;
using pruebatoka.dto;
using pruebatoka.Models;
using pruebatoka.Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebatoka.Servicios
{
    public class PersonaFisicaServicio : IPersonaFisicaServicio
    {
        private readonly pruebatokaContext _context;


        public PersonaFisicaServicio(pruebatokaContext context )
        {
            _context = context;
        }
        public async Task<ActionResult<IEnumerable<PersonaFisica>>> Obtener()
        {
            return await _context.Tb_PersonasFisicas.Where(p => p.Activo == true).ToListAsync();

        }
        public PersonaFisica ObtenerPorId(int id)
        {
            //  var personaFisica = await _context.Tb_PersonasFisicas.FindAsync(id);
            var personaFisica =  _context.Tb_PersonasFisicas.FirstOrDefault(p => p.IdPersonaFisica == id && p.Activo == true);
        

            return personaFisica;
        }
        public RespuestaGenericaDto Insertar(PersonaFisicaInsertDto data)
        {
            RespuestaGenericaDto result = new RespuestaGenericaDto()
            {
                MENSAJEERROR = null,
                ERROR = 0
            };
            try
            {

            using (SqlConnection conn = (SqlConnection)_context.Database.GetDbConnection())
            {
         
            SqlCommand cmd = conn.CreateCommand();
            conn.Open();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_AgregarPersonaFisica";
 
            cmd.Parameters.Add("@Nombre",System.Data.SqlDbType.VarChar,50).Value = data.Nombre;
            cmd.Parameters.Add("@ApellidoPaterno", System.Data.SqlDbType.VarChar, 50).Value = data.ApellidoPaterno;
            cmd.Parameters.Add("@ApellidoMaterno", System.Data.SqlDbType.VarChar, 50).Value = data.ApellidoMaterno;
            cmd.Parameters.Add("@RFC", System.Data.SqlDbType.VarChar, 50).Value= data.RFC;
            cmd.Parameters.Add("@FechaNacimiento", System.Data.SqlDbType.Date).Value = data.FechaNacimiento;
            cmd.Parameters.Add("@UsuarioAgrega", System.Data.SqlDbType.Int).Value = data.UsuarioAgrega;

              var reader = cmd.ExecuteReader();

                if (reader.Read())
                {

                    result.MENSAJEERROR= reader["MENSAJEERROR"].ToString();
                    result.ERROR = Convert.ToInt32(reader["ERROR"]);
                }
                reader.Close();
            }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                result.MENSAJEERROR = "error al insertar persona fisica";
                result.ERROR = -5000;
            }
            return result;

        }
        public RespuestaGenericaDto Actualizar(PersonaFisicaActualizarDto data)
        {
            RespuestaGenericaDto result = new RespuestaGenericaDto()
            {
                MENSAJEERROR = null,
                ERROR = 0
            };
            try
            {

                using (SqlConnection conn = (SqlConnection)_context.Database.GetDbConnection())
                {

                    SqlCommand cmd = conn.CreateCommand();
                    conn.Open();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_ActualizarPersonaFisica";
                    cmd.Parameters.Add("@IdPersonaFisica", System.Data.SqlDbType.Int).Value = data.IdPersonaFisica;
                    cmd.Parameters.Add("@Nombre", System.Data.SqlDbType.VarChar, 50).Value = data.Nombre;
                    cmd.Parameters.Add("@ApellidoPaterno", System.Data.SqlDbType.VarChar, 50).Value = data.ApellidoPaterno;
                    cmd.Parameters.Add("@ApellidoMaterno", System.Data.SqlDbType.VarChar, 50).Value = data.ApellidoMaterno;
                    cmd.Parameters.Add("@RFC", System.Data.SqlDbType.VarChar, 50).Value = data.RFC;
                    cmd.Parameters.Add("@FechaNacimiento", System.Data.SqlDbType.Date).Value = data.FechaNacimiento;
                    cmd.Parameters.Add("@UsuarioAgrega", System.Data.SqlDbType.Int).Value = data.UsuarioAgrega;

                    var reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                       
                        result.MENSAJEERROR = reader["MENSAJEERROR"].ToString();
                        result.ERROR = Convert.ToInt32(reader["ERROR"]);
                    }
                    reader.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                result.MENSAJEERROR = "error al actualizar persona fisica";
                result.ERROR = -5000;
            }
            return result;

        }
        public RespuestaGenericaDto Eliminar(int id)
        {
            RespuestaGenericaDto result = new RespuestaGenericaDto()
            {
                MENSAJEERROR = null,
                ERROR = 0
            };
            try
            {

                using (SqlConnection conn = (SqlConnection)_context.Database.GetDbConnection())
                {

                    SqlCommand cmd = conn.CreateCommand();
                    conn.Open();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "sp_EliminarPersonaFisica";
                    cmd.Parameters.Add("@IdPersonaFisica", System.Data.SqlDbType.Int).Value = id;

                    var reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {

                        result.MENSAJEERROR = reader["MENSAJEERROR"].ToString();
                        result.ERROR = Convert.ToInt32(reader["ERROR"]);
                    }
                    reader.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                result.MENSAJEERROR = "error al eliminar persona fisica";
                result.ERROR = -5000;
            }
            return result;

        }
    }
}
