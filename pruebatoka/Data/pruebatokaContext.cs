﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pruebatoka.Models;

namespace pruebatoka.Data
{
    public class pruebatokaContext : DbContext
    {
        public pruebatokaContext (DbContextOptions<pruebatokaContext> options)
            : base(options)
        {
        }

        public DbSet<pruebatoka.Models.PersonaFisica> Tb_PersonasFisicas { get; set; }
    }
}
