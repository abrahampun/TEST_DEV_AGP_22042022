﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pruebatoka.Data;
using pruebatoka.dto;
using pruebatoka.Models;
using pruebatoka.Servicios.Interfaces;

namespace pruebatoka.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaFisicaController : ControllerBase
    {
   
        private readonly IPersonaFisicaServicio _personaFisicaServicio;
        public PersonaFisicaController( IPersonaFisicaServicio personaFisicaServicio)
        {   
            _personaFisicaServicio = personaFisicaServicio;
        }

        // GET: api/PersonaFisicas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonaFisica>>> GetPersonaFisica()
        {
          return await  _personaFisicaServicio.Obtener();
        }

        // GET: api/PersonaFisicas/5
        [HttpGet("{id}")]
        public  ActionResult<PersonaFisica> GetPersonaFisica(int id)
        {
        
            var personaFisica = _personaFisicaServicio.ObtenerPorId(id);
            if (personaFisica == null)
            {
                return NotFound();
            }

            return personaFisica;
        }

        // PUT: api/PersonaFisicas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public ActionResult<RespuestaGenericaDto> PutPersonaFisica(PersonaFisicaActualizarDto personaFisica)
        {
            var resultado = _personaFisicaServicio.Actualizar(personaFisica);

            if (resultado.ERROR < 0)
            {
                return BadRequest(resultado);
            }
            return resultado;
        }

        // POST: api/PersonaFisicas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public  ActionResult<RespuestaGenericaDto> PostPersonaFisica(PersonaFisicaInsertDto personaFisica)
        {
            var resultado = _personaFisicaServicio.Insertar(personaFisica);
            if (resultado.ERROR < 0 )
            {
                return BadRequest(resultado);
            }
            return resultado;
       
        }

        // DELETE: api/PersonaFisicas/5
        [HttpDelete("{id}")]
        public ActionResult<RespuestaGenericaDto>  DeletePersonaFisica(int id)
        {
            var resultado = _personaFisicaServicio.Eliminar(id);
            if (resultado.ERROR < 0)
            {
                return BadRequest(resultado);
            }
            return resultado;
        }

    }
}
