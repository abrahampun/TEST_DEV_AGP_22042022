﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebatoka.dto
{
    public class RespuestaGenericaDto
    {
        public string MENSAJEERROR { get; set; }
        public int ERROR { get; set; }
    }
}
