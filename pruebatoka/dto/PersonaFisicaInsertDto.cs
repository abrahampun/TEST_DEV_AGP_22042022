﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pruebatoka.dto
{
    public class PersonaFisicaInsertDto
    {
        [Required(ErrorMessage = "Nombre Requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Apellido Paterno Requerido")]
        public string ApellidoPaterno { get; set; }
        [Required(ErrorMessage = "Apellido Materno Requerido")]
        public string ApellidoMaterno { get; set; }

        [Required(ErrorMessage = "RFC Requerido")]
        [StringLength(13, ErrorMessage = "RFC debe contar con 13 caracteres", MinimumLength = 13)]
        public string RFC { get; set; }
        [Required(ErrorMessage = "Fecha de Nacimiento Requerido")]
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage = "Error usuario no identificado")]
        public int UsuarioAgrega { get; set; }

    }
}
